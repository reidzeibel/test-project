package com.reidzeibel.qasirtestapp.views.adapters.viewholders

import android.view.View
import com.bumptech.glide.Glide
import com.reidzeibel.qasirtestapp.base.BindableViewHolder
import com.reidzeibel.qasirtestapp.data.remote.product.Product
import com.reidzeibel.qasirtestapp.utils.CurrencyUtil
import kotlinx.android.synthetic.main.item_product.view.*

class ProductViewHolder(itemView: View) : BindableViewHolder<Product>(itemView) {

    override fun bind(item: Product) {
        super.bind(item)
        with(itemView) {
            productName.text = item.productName
            productPrice.text = CurrencyUtil.formatCurrency(item.price)
            productStock.text = item.stock.toString()
            Glide.with(itemView.context)
                .load(item.images.thumbnail)
                .into(productThumbnail)
        }
    }
}
