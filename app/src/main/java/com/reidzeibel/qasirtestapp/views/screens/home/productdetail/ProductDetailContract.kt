package com.reidzeibel.qasirtestapp.views.screens.home.productdetail

import com.reidzeibel.qasirtestapp.base.BasePresenter
import com.reidzeibel.qasirtestapp.base.BaseView
import com.reidzeibel.qasirtestapp.data.remote.product.Product

interface ProductDetailContract {

    interface View: BaseView {

        fun onGetProductSuccess(product: Product)

        fun onGetProductFailed(error: String)

    }

    interface Presenter: BasePresenter<View> {

        fun getProduct(productId: Long)

    }

}