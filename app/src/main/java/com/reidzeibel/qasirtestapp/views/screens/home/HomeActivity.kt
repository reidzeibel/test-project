package com.reidzeibel.qasirtestapp.views.screens.home

import android.view.MenuItem
import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.base.BaseActivity
import com.reidzeibel.qasirtestapp.di.components.DaggerHomeComponent
import com.reidzeibel.qasirtestapp.di.components.HomeComponent
import com.reidzeibel.qasirtestapp.views.screens.home.productdetail.ProductDetailFragment
import com.reidzeibel.qasirtestapp.views.screens.home.productlist.ProductListFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity<HomeComponent>(), ProductListFragment.OnProductSelectedListener {

    override fun getLayout(): Int = R.layout.activity_home

    override fun initInjector(): HomeComponent = DaggerHomeComponent.builder().build()

    override fun inject(component: HomeComponent) {
        component.inject(this)
    }

    override fun initViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Order Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
        if (container != null) {
            addFragment(navigator.getProductListFragment(this))
        }
    }

    override fun onProductSelected(productId: Long) {
        val detailFragment = supportFragmentManager.findFragmentById(R.id.detailFragment) as ProductDetailFragment?
        if (detailFragment != null) {
            detailFragment.updateProduct(productId)
        } else {
            addFragment(navigator.getProductDetailFragment(this, productId))
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.findFragmentByTag(ProductDetailFragment::class.java.simpleName) != null) {
            addFragment(navigator.getProductListFragment(this))
            supportActionBar?.title = "Order Barang"
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(false)
        } else {
            super.onBackPressed()
        }
    }
}