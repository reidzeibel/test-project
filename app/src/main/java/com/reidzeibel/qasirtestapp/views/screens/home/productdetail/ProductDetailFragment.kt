package com.reidzeibel.qasirtestapp.views.screens.home.productdetail

import android.content.res.Configuration
import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import com.bumptech.glide.Glide
import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.base.BaseFragment
import com.reidzeibel.qasirtestapp.data.remote.product.Product
import com.reidzeibel.qasirtestapp.di.components.DaggerProductComponent
import com.reidzeibel.qasirtestapp.di.components.ProductComponent
import com.reidzeibel.qasirtestapp.di.modules.ProductModule
import com.reidzeibel.qasirtestapp.utils.CurrencyUtil
import kotlinx.android.synthetic.main.fragment_detail.*
import javax.inject.Inject

class ProductDetailFragment: BaseFragment<ProductComponent>(), ProductDetailContract.View {

    @Inject
    lateinit var presenter: ProductDetailContract.Presenter

    override fun getLayout(): Int = R.layout.fragment_detail

    override fun initInjector(): ProductComponent =
        DaggerProductComponent.builder()
            .applicationComponent(applicationComponent)
            .productModule(ProductModule())
            .build()

    override fun injectFragment(component: ProductComponent) {
        component.inject(this)
    }

    override fun initViews() {
        addPresenter(presenter, this)
        if (getProductId() != -1L) {
            updateProduct(getProductId())
        } else {
            updateProduct(1)
        }
    }

    override fun onGetProductSuccess(product: Product) {
        Glide.with(this).load(product.images.large).into(productImage)
        productTitle.text = product.productName
        productPrice.text = CurrencyUtil.formatCurrency(product.price)
        productDescription.text = HtmlCompat.fromHtml(product.description, FROM_HTML_MODE_LEGACY)
        if (activity().resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            activity().supportActionBar?.title = product.productName
        }
    }

    override fun onGetProductFailed(error: String) {
        showInAppError("Failed getting Product", error)
    }

    fun updateProduct(productId: Long) {
        presenter.getProduct(productId)
    }

    private fun getProductId(): Long {
        return if (arguments != null && arguments!!.containsKey(ARGS_PRODUCT_ID))
            arguments!!.getLong(ARGS_PRODUCT_ID, -1L)
        else -1L
    }

    companion object {
        private const val ARGS_PRODUCT_ID: String = "args:product_id"

        @JvmStatic
        fun newInstance(productId: Long) : ProductDetailFragment{
            val productDetailFragment = ProductDetailFragment()
            val bundle = Bundle()
            bundle.putLong(ARGS_PRODUCT_ID, productId)
            productDetailFragment.arguments = bundle
            return productDetailFragment
        }

    }
}