package com.reidzeibel.qasirtestapp.views.screens.home.productlist

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.base.BaseFragment
import com.reidzeibel.qasirtestapp.data.remote.product.ProductData
import com.reidzeibel.qasirtestapp.di.components.DaggerProductComponent
import com.reidzeibel.qasirtestapp.di.components.ProductComponent
import com.reidzeibel.qasirtestapp.di.modules.ProductModule
import com.reidzeibel.qasirtestapp.views.adapters.ProductItemDecorator
import com.reidzeibel.qasirtestapp.views.adapters.ProductListAdapter
import kotlinx.android.synthetic.main.fragment_list.*
import timber.log.Timber
import javax.inject.Inject

class ProductListFragment : BaseFragment<ProductComponent>(), ProductListContract.View {

    @Inject
    lateinit var presenter: ProductListContract.Presenter

    @Inject
    lateinit var productListAdapter: ProductListAdapter

    private lateinit var listener: OnProductSelectedListener

    override fun getLayout(): Int = R.layout.fragment_list

    override fun initInjector(): ProductComponent =
        DaggerProductComponent.builder()
            .applicationComponent(applicationComponent)
            .productModule(ProductModule())
            .build()

    override fun injectFragment(component: ProductComponent) = component.inject(this)

    override fun initViews() {
        addPresenter(presenter, this)
        productList.layoutManager = LinearLayoutManager(context)
        productList.adapter = productListAdapter
        productList.addItemDecoration(ProductItemDecorator())
        addDisposables(productListAdapter.singleClickPublish.subscribe(
            { product ->
                listener.onProductSelected(product.productId)
            },
            { throwable ->
                Timber.e(throwable)
            }
        )
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as OnProductSelectedListener
    }

    override fun onGetProductSuccess(productData: ProductData) {
        productListAdapter.setProducts(productData.products)
        Glide.with(this).load(productData.banner.image).into(banner)
    }

    override fun onGetProductFailed(error: String) {
        showInAppError("Failed Getting Product List", error)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ProductListFragment()
    }

    interface OnProductSelectedListener {

        fun onProductSelected(productId: Long)

    }
}