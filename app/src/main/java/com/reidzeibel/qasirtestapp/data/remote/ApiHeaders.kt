package com.reidzeibel.qasirtestapp.data.remote

import okhttp3.Interceptor
import okhttp3.Response
import com.reidzeibel.qasirtestapp.data.local.SharedPreferenceHelper
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class to add API Header on the fly, particularly useful when using bearer tokens
 */

@Singleton
class ApiHeaders @Inject constructor(private val sharedPreferenceHelper: SharedPreferenceHelper) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val builder = original.newBuilder()
                .addHeader("Content-Type", CONTENT_TYPE)

        if (getToken().isNotBlank()) {
            builder.addHeader("Authorization", "Bearer ${getToken()}")
        }

        builder.method(original.method(), original.body())
        val request = builder.build()
        return chain.proceed(request)
    }

    private fun getToken(): String {
        return sharedPreferenceHelper.getToken()
    }

    companion object {
        const val CONTENT_TYPE = "application/json; charset=utf-8"
    }

}
