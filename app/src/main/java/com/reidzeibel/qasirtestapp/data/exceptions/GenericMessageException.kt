package com.reidzeibel.qasirtestapp.data.exceptions

open class GenericMessageException(
        private val errorMessage: String) : RuntimeException() {
    open val title: String
        get() = ""
    override val message: String?
        get() = errorMessage
}