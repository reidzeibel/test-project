package com.reidzeibel.qasirtestapp.data.remote.product

import com.reidzeibel.qasirtestapp.data.exceptions.ProductNotFoundException
import com.reidzeibel.qasirtestapp.data.remote.parseError
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductManagerImpl @Inject constructor(
    private val productApi: ProductApi
) : ProductManager {

    private lateinit var productData: ProductData

    override fun getAllProducts(): Single<ProductData> =
        if (::productData.isInitialized && productData.products.isNotEmpty()) {
            Single.just(productData)
        } else {
            productApi.getAllProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { productData = it.data }
                .map { it.data }
                .parseError()
        }

    override fun getProduct(productId: Long): Single<Product> {
        return if (productData.products.contains(productId)) {
            Single.just(productData.products.get(productId))
        } else {
            Single.error(ProductNotFoundException(productId))
        }
    }
}
