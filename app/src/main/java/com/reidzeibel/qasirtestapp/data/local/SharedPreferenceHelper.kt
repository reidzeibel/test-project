package com.reidzeibel.qasirtestapp.data.local

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferenceHelper @Inject constructor(context: Context) {

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)

    // for saving and getting token
    fun setToken(token: String) = sharedPreferences.edit { putString(TOKEN, token) }
    fun getToken() : String = sharedPreferences.getString(TOKEN, "")!!

    fun clear() = sharedPreferences.edit {
        clear()
    }


    companion object {

        private const val PREFERENCE_KEY = "qasir_preferences"

        private const val TOKEN = "token"

    }

}