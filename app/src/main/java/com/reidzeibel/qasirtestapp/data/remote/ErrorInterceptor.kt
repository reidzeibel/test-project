package com.reidzeibel.qasirtestapp.data.remote

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.reidzeibel.qasirtestapp.data.exceptions.GenericMessageException
import com.reidzeibel.qasirtestapp.data.exceptions.ServerException
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ErrorInterceptor @Inject constructor(var gson: Gson) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        if (!response.isSuccessful) {
            throw parseError(response)
        }

        return response
    }

    private fun parseError(response: Response): RuntimeException {
        return try {
            val body = response.body()
            val bodyString = body!!.string()
            val error = gson.fromJson(bodyString, APIError::class.java)
            GenericMessageException(error.message)
        } catch (e: Exception) {
            ServerException()
        }
    }
}


class APIError(@SerializedName(value = "message", alternate = ["msg"]) val message: String)