package com.reidzeibel.qasirtestapp.data.remote

import io.reactivex.*
import io.reactivex.functions.Function
import timber.log.Timber
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

fun <R> Observable<R>.parseError(): Observable<R> {
    return this.onErrorResumeNext(errorCasting())
}

fun Completable.parseError(): Completable {
    return this.onErrorResumeNext(errorCompletableCasting())
}

fun <R> Single<R>.parseError(): Single<R> {
    return this.onErrorResumeNext(errorSingleCasting())
}

private fun <T> errorCasting(): io.reactivex.functions.Function<Throwable, ObservableSource<T>> {
    return Function { throwable : Throwable ->
        val output: Throwable = parseThrowable(throwable)
        Observable.error<T>(output)
    }
}

private fun <T> errorSingleCasting(): io.reactivex.functions.Function<Throwable, SingleSource<T>> {
    return Function { throwable : Throwable ->
        val output: Throwable = parseThrowable(throwable)
        Single.error<T>(output)
    }
}

private fun parseThrowable(throwable: Throwable): Throwable {
    var output: Throwable = throwable
    when (throwable) {
        is TimeoutException -> output = TimeoutException("Connection timeout, please check your network connection and try again")
        is SocketTimeoutException -> output = TimeoutException("Connection timeout, please check your network connection and try again")
        is UnknownHostException -> output = UnknownHostException("There is no Internet connection available, please make sure that you are connected to the Internet")
        else -> Timber.e(throwable)
    }
    return output
}

private fun errorCompletableCasting(): io.reactivex.functions.Function<Throwable, Completable> {
    return Function { throwable : Throwable ->
        val output: Throwable = parseThrowable(throwable)
        Completable.error(output)
    }
}
