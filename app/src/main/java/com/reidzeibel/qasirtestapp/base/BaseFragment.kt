package com.reidzeibel.qasirtestapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.reidzeibel.qasirtestapp.di.components.ApplicationComponent
import com.reidzeibel.qasirtestapp.views.screens.Navigator
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

abstract class BaseFragment<T> : Fragment() {

    private var presenters: ArrayList<BasePresenter<*>> = ArrayList()
    private var observerList: ArrayList<Observable<Boolean>> = ArrayList()
    protected var navigator: Navigator = Navigator()
    private var disposables: Array<Disposable>? = null

    init {
        retainInstance = true
    }

    fun activity(): BaseActivity<*> {
        return activity as BaseActivity<*>
    }

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun initInjector(): T

    abstract fun injectFragment(component: T)

    abstract fun initViews()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val component: T = initInjector()
        injectFragment(component)
        if (savedInstanceState == null)
            initViews()
    }


    protected val applicationComponent: ApplicationComponent
        get() = activity().getApp().appComponent


    fun <T : BaseView> addPresenter(presenter: BasePresenter<T>, view: T) {
        presenter.setTargetView(view)
        presenters.add(presenter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenters.forEach { it.subscribe() }
    }

    override fun onResume() {
        super.onResume()
        presenters.forEach { it.onResume() }
    }

    override fun onStop() {
        super.onStop()
        presenters.forEach { it.unsubscribe() }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenters.forEach { it.onDestroy() }
        disposables?.forEach { it.dispose() }
    }

    fun showInAppError(title: String, message: String, callback: Callback?) {
        AlertDialog.Builder(activity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(android.R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setOnDismissListener {
                    callback?.onDismissed()
                }
                .show()
    }

    interface Callback {
        fun onDismissed()
    }

    fun showInAppError(title: String, message: String) {
        showInAppError(title, message, null)
    }

    fun addDisposables(vararg disposables: Disposable) {
        this.disposables = arrayOf(*disposables)
    }
}