package com.reidzeibel.qasirtestapp.base

import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.reidzeibel.qasirtestapp.QasirApplication
import com.reidzeibel.qasirtestapp.R
import com.reidzeibel.qasirtestapp.di.components.ApplicationComponent
import com.reidzeibel.qasirtestapp.views.screens.Navigator
import io.reactivex.disposables.Disposable

abstract class BaseActivity<T> : AppCompatActivity() {

    private var presenters: ArrayList<BasePresenter<*>> = ArrayList()
    protected var navigator: Navigator = Navigator()
    private var disposables: Array<Disposable>? = null

    private val applicationComponent: ApplicationComponent
        get() = (application as QasirApplication).appComponent

    fun addFragment(fragment: Fragment, addStack: Boolean = false) {
        replaceFragmentInActivity(fragment, R.id.container, addStack)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())

        val component : T = initInjector()
        inject(component)

        initViews()
    }

    override fun onStart() {
        super.onStart()
        presenters.forEach { it.subscribe() }
    }

    override fun onResume() {
        super.onResume()
        presenters.forEach { it.onResume() }
    }

    override fun onStop() {
        super.onStop()
        presenters.forEach { it.unsubscribe() }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenters.forEach { it.onDestroy() }
        disposables?.forEach { it.dispose() }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun getApp(): QasirApplication{
        return application as QasirApplication
    }

    fun <T : BaseView> addPresenter(presenter: BasePresenter<T>, view: T) {
        presenter.setTargetView(view)
        presenters.add(presenter)
    }

    fun addDisposables(vararg disposables: Disposable) {
        this.disposables = arrayOf(*disposables)
    }

    interface Callback {
        fun onDismissed()
    }

    @LayoutRes
    abstract fun getLayout(): Int

    abstract fun initInjector() : T

    abstract fun inject(component: T)

    abstract fun initViews()

    fun showInAppError(title: String, message: String, callback: Callback?) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(getString(android.R.string.ok)) { dialog, _ ->
                dialog.dismiss()
            }
            .setOnDismissListener {
                callback?.onDismissed()
            }
            .show()
    }

    fun showInAppError(title: String, message: String) {
        showInAppError(title, message, null)
    }

    private fun replaceFragmentInActivity(fragment: Fragment, @IdRes frameId: Int, addStack: Boolean) {
        supportFragmentManager.transact {
            replace(frameId, fragment, fragment::class.java.simpleName)
            if (addStack)
                addToBackStack(fragment::class.java.simpleName)
        }
    }

    private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
        beginTransaction().apply {
            action()
        }.commit()
    }
}