package com.reidzeibel.qasirtestapp

import android.app.Application
import com.reidzeibel.qasirtestapp.di.components.ApplicationComponent
import com.reidzeibel.qasirtestapp.di.components.DaggerApplicationComponent
import com.reidzeibel.qasirtestapp.di.modules.ApplicationModule
import timber.log.Timber

class QasirApplication : Application() {

    val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}