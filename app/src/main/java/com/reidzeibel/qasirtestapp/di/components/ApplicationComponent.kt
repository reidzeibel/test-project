package com.reidzeibel.qasirtestapp.di.components

import android.app.Application
import android.content.Context
import com.reidzeibel.qasirtestapp.data.local.SharedPreferenceHelper
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManager
import com.reidzeibel.qasirtestapp.di.modules.ApiModule
import com.reidzeibel.qasirtestapp.di.modules.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ApiModule::class])
interface ApplicationComponent {

    fun application(): Application

    fun context(): Context

    fun sessionManager() : ProductManager

    fun sharedPreferenceHelper() : SharedPreferenceHelper
}