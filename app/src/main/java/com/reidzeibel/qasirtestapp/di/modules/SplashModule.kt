package com.reidzeibel.qasirtestapp.di.modules

import com.reidzeibel.qasirtestapp.di.PerActivity
import com.reidzeibel.qasirtestapp.views.screens.splash.SplashContract
import com.reidzeibel.qasirtestapp.views.screens.splash.SplashPresenter
import dagger.Module
import dagger.Provides

@Module
class SplashModule {

    @Provides
    @PerActivity
    fun provideSplashPresenter(splashPresenter: SplashPresenter) : SplashContract.Presenter = splashPresenter

}