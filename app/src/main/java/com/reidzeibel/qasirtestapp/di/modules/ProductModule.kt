package com.reidzeibel.qasirtestapp.di.modules

import com.reidzeibel.qasirtestapp.di.PerFragment
import com.reidzeibel.qasirtestapp.views.screens.home.productdetail.ProductDetailContract
import com.reidzeibel.qasirtestapp.views.screens.home.productdetail.ProductDetailPresenter
import com.reidzeibel.qasirtestapp.views.screens.home.productlist.ProductListContract
import com.reidzeibel.qasirtestapp.views.screens.home.productlist.ProductListPresenter
import dagger.Module
import dagger.Provides

@Module
class ProductModule  {

    @Provides
    @PerFragment
    fun provideProductListPresenter(productListPresenter: ProductListPresenter) : ProductListContract.Presenter = productListPresenter

    @Provides
    @PerFragment
    fun provideProductDetailPresenter(productDetailPresenter: ProductDetailPresenter) : ProductDetailContract.Presenter = productDetailPresenter

}