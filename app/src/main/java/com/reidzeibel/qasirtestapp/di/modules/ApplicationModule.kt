package com.reidzeibel.qasirtestapp.di.modules

import android.app.Application
import android.content.Context
import com.reidzeibel.qasirtestapp.data.local.SharedPreferenceHelper
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManager
import com.reidzeibel.qasirtestapp.data.remote.product.ProductManagerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val mApplication: Application) {

    @Provides
    @Singleton
    internal fun provideApplication() = mApplication

    @Provides
    @Singleton
    internal fun provideContext() = mApplication as Context

    @Provides
    @Singleton
    internal fun provideProductManager(productManager: ProductManagerImpl) : ProductManager = productManager

    @Provides
    @Singleton
    internal fun provideSharedPreferenceHelper() = SharedPreferenceHelper(mApplication as Context)
}