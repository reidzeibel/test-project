package com.reidzeibel.qasirtestapp.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.reidzeibel.qasirtestapp.BuildConfig
import com.reidzeibel.qasirtestapp.data.local.SharedPreferenceHelper
import com.reidzeibel.qasirtestapp.data.remote.ApiHeaders
import com.reidzeibel.qasirtestapp.data.remote.ErrorInterceptor
import com.reidzeibel.qasirtestapp.data.remote.product.ProductApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideGsonConverter(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun provideErrorInterceptor(gson: Gson) = ErrorInterceptor(gson)

    @Provides
    @Singleton
    internal fun provideClient(headers: ApiHeaders, errorInterceptor: ErrorInterceptor) = OkHttpClient.Builder()
            .addInterceptor(headers)
            .addInterceptor(errorInterceptor)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()


    @Provides
    @Singleton
    internal fun provideRetrofit(client: OkHttpClient, converter: GsonConverterFactory) = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL_API)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(converter)
            .build()

    @Provides
    @Singleton
    internal fun provideApiHeaders(sharedPreferenceHelper: SharedPreferenceHelper) = ApiHeaders(sharedPreferenceHelper)

    @Provides
    @Singleton
    internal fun provideProductApi(retrofit: Retrofit) = retrofit.create(ProductApi::class.java)


}